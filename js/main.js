$(document).ready(function() {
    let jsonUrl = "";
    let authToken = "?key=708a24f98ac0f1529100dec29f4cbc99&token=6580601a5882c52eea871796c98bc49dbb5e8c98d3d99795bee5f3ba19aadced"
    let boards = []
    var data;
    let boardsRequestUrl = 'https://api.trello.com/1/members/me/boards'+authToken

    $.getJSON(boardsRequestUrl, (jsonData)=>{
        jsonData.map(board=>{
            // console.log(board.id)
            boards.push([board.name, board.id])
            let cardUrl = 'https://api.trello.com/1/boards/'+board.id+'/cards'+authToken
            $.getJSON(cardUrl, (cards)=>{
                // console.log(card["id"])
                cards.map((card)=>{
                    let listUrl = 'https://api.trello.com/1/cards/'+card.id+'/checklists'+authToken
                    $.getJSON(listUrl, (list)=>{
                        let items = []
                        list.map((object)=>{
                            let checkItems = object.checkItems
                            // console.log(checkItems)
                            checkItems.map(checkListItem=>{
                                let tempObject = {};
                                // console.log(eachProp)
                                tempObject["name"] = checkListItem.name
                                tempObject["id"] = checkListItem.id
                                tempObject["state"] = checkListItem.state
                                tempObject["boardId"] = board.id
                                tempObject["cardId"] = card.id
                                tempObject["cardName"] = card.name
                                tempObject["idCheckList"] = checkListItem.idChecklist
                                // console.log(eachProp.idChecklist)
                                items.push(tempObject)
                            })
                        })
                        
                        items.forEach(item=>{
                            if(item.state==="incomplete"){
                                var id = item.cardId+"-"+item.id+"-"+item.idCheckList
                                    var labelContent = item.name;
                                    var labelCreatedDate = item.cardName
                                    var article = '<li data-id="'+ id + '"' +'"><article class="task display-flex flex-direction-row flex-space-between align-items-center"><section class="display-flex"><input type="checkbox" id='+'"'+id+'"'+'><label for='
                                    +'"'+id+'"'+'>'+labelContent+'         '+'<span class="createdDateHighlight">'+labelCreatedDate+'</span>'+'</label></section><a href="#" class="identifier"data-id="' + id + '"' +'class="delete-todo"><i id="'+id+'"class="fa fa-times" aria-hidden="true"></i></a></article></li>'
                                    $('#todo-list').append(article)
                            }
                        })
                    })
                })
            })
        })
    })

    // updating the trello api
    console.log(boards)
    $(document).on("change", "input[type=checkbox]", function () {
        if (this.checked) {
            var id = this.id.split('-')
            var data = null;

            // var xhr = new XMLHttpRequest();

            // xhr.addEventListener("readystatechange", function () {
            //     if (this.readyState === this.DONE) {
            //         console.log(this.responseText);
            //     }
            //     });

            // xhr.open("PUT", 'https://api.trello.com/1/cards/'+id[0]+'/checkItem/'+id[1]+'?state=complete&key=708a24f98ac0f1529100dec29f4cbc99&token=6580601a5882c52eea871796c98bc49dbb5e8c98d3d99795bee5f3ba19aadced');

            // xhr.send(data);
            // window.location.reload(1);
            $.ajax({
                url: `https://api.trello.com/1/cards/${id[0]}/checkItem/${id[1]}?state=complete&key=708a24f98ac0f1529100dec29f4cbc99&token=6580601a5882c52eea871796c98bc49dbb5e8c98d3d99795bee5f3ba19aadced`, // your api url
                method: 'PUT',
                data: {},
                success: function() {}
            });

        }
        else{
            // var id = this.id.split('-')
            // var data = null;

            // var xhr = new XMLHttpRequest();

            // xhr.addEventListener("readystatechange", function () {
            //     if (this.readyState === this.DONE) {
            //         console.log(this.responseText);
            //     }
            //     });

            // xhr.open("PUT", 'https://api.trello.com/1/cards/'+id[0]+'/checkItem/'+id[1]+'?state=incomplete&key=708a24f98ac0f1529100dec29f4cbc99&token=6580601a5882c52eea871796c98bc49dbb5e8c98d3d99795bee5f3ba19aadced');

            // xhr.send(data);
            // window.location.reload(1);
            $.ajax({
                url: `https://api.trello.com/1/cards/${id[0]}/checkItem/${id[1]}?state=incomplete&key=708a24f98ac0f1529100dec29f4cbc99&token=6580601a5882c52eea871796c98bc49dbb5e8c98d3d99795bee5f3ba19aadced`, // your api url
                method: 'PUT',
                data: {},
                success: function() {}
            });
            
        }
    });

    

































    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //listening for the button click
    $('#add-todo').click(()=>{
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                           ];
        const date = new Date();
        const todayCardName = monthNames[date.getMonth()]+" "+date.getDate()
        // const todayCardName = "Sep 7"
        // console.log(boards)
        let textBoxContent = $('#todo-input').val();
        if(textBoxContent==""){
            alert("please add something to do")
        }
        // let id = "5a3"
        // // var temp = 'id='+'"'+id+'"';
        // var article = '<li><article class="display-flex flex-direction-row flex-space-between align-items-center"><section class="display-flex"><input type="checkbox" id='+'"'+id+'"'+'><label for='
        // +'"'+id+'"'+'>'+textBoxContent+'</label></section><a href="#" class ="delete-todo"><i class="fa fa-times" aria-hidden="true"></i></a></article></li>'
        // if(textBoxContent!="") 
        //     {
        //         $('#todo-list').append(article)
        //         $('#todo-input').val("")
        //     }
        // else{
        //     alert("please enter something to do..")
        // }
        boards.map(board=>{
            if(board[0]=="Ajay Kumar"){
                let listUrl = `https://api.trello.com/1/boards/${board[1]}/lists${authToken}`
                $.getJSON(listUrl, lists=>{
                    lists.map(list=>{
                        if(list.name=="To do"){
                            // console.log("list id is "+ list.id)
                            let cardUrl = `https://api.trello.com/1/lists/${list.id}/cards${authToken}`
                            $.getJSON(cardUrl,(cards)=>{
                                let cardsInfo = {}
                                cards.forEach(card=>{
                                    // console.log(card)
                                    cardsInfo[card.name] = {}
                                    cardsInfo[card.name]["id"] = card.id
                                    cardsInfo[card.name]["idChecklist"] = card.idChecklists[0]
                                })
                                let cardsNames = Object.keys(cardsInfo);
                                // console.log(cardsNames)
                                // console.log(cardsInfo)
                                if(cardsNames.includes(todayCardName)){
                                    // console.log(card)
                                        let cardNameProp = cardsInfo[todayCardName]
                                        // console.log(cardNameProp)
                                        let textBoxContent = $('#todo-input').val();
                                        let currentCardId = cardNameProp["id"]
                                        let currentCardChecklistId = cardNameProp["idChecklist"]
                                        // console.log("checklist id "+currentCardChecklistId)
                                        // console.log("card id is "+currentCardId)
                                        let itemPostUrl = `https://api.trello.com/1/checklists/${currentCardChecklistId}/checkItems?name=${textBoxContent}&pos=bottom&key=708a24f98ac0f1529100dec29f4cbc99&token=6580601a5882c52eea871796c98bc49dbb5e8c98d3d99795bee5f3ba19aadced`
                                        $.post(itemPostUrl, function( response ) {
                                            // console.log(response)
                                            // console.log("new item is "+ response.id)
                                            let labelContent = textBoxContent
                                            let labelCreatedDate = todayCardName
                                            let id = currentCardId+"-"+response.id+"-"+currentCardChecklistId
                                            var article = '<li data-id="'+ id + '"' +'"><article class="task display-flex flex-direction-row flex-space-between align-items-center"><section class="display-flex"><input type="checkbox" id='+'"'+id+'"'+'><label for='
                                            +'"'+id+'"'+'>'+labelContent+'         '+'<span class="createdDateHighlight">'+labelCreatedDate+'</span>'+'</label></section><a href="#" class="identifier"data-id="' + id + '"' +'class="delete-todo"><i id="'+id+'"class="fa fa-times" aria-hidden="true"></i></a></article></li>'
                                            $('#todo-list').append(article)
                                            $('#todo-input').val("")
                                        });
                                }
                                else{
                                    let cardUrl = `https://api.trello.com/1/cards?name=${todayCardName}&idList=${list.id}&keepFromSource=all&key=708a24f98ac0f1529100dec29f4cbc99&token=6580601a5882c52eea871796c98bc49dbb5e8c98d3d99795bee5f3ba19aadced`
                                        $.post( cardUrl, function( response ) {
                                            let newCardId = response.id
                                            let checkListUrl = `https://api.trello.com/1/cards/${newCardId}/checklists?key=708a24f98ac0f1529100dec29f4cbc99&token=6580601a5882c52eea871796c98bc49dbb5e8c98d3d99795bee5f3ba19aadced`
                                            $.post( checkListUrl, function( response ) {
                                                let newChecklistId = response.id
                                                let itemPostUrl = `https://api.trello.com/1/checklists/${newChecklistId}/checkItems?name=${textBoxContent}&pos=bottom&key=708a24f98ac0f1529100dec29f4cbc99&token=6580601a5882c52eea871796c98bc49dbb5e8c98d3d99795bee5f3ba19aadced`
                                                $.post(itemPostUrl, function( response ) {
                                                    // console.log(response)
                                                    // console.log("new card id is "+newCardId)
                                                    // console.log("new check list id is "+newChecklistId)
                                                    // console.log("new item is "+ response.id)
                                                    let labelContent = textBoxContent;
                                                    let labelCreatedDate = todayCardName
                                                    let id = newCardId+"-"+response.id+"-"+newChecklistId
                                                    var article = '<li data-id="'+ id + '"' +'"><article class="task display-flex flex-direction-row flex-space-between align-items-center"><section class="display-flex"><input type="checkbox" id='+'"'+id+'"'+'><label for='
                                                    +'"'+id+'"'+'>'+labelContent+'         '+'<span class="createdDateHighlight">'+labelCreatedDate+'</span>'+'</label></section><a href="#" class="identifier"data-id="' + id + '"' +'class="delete-todo"><i id="'+id+'"class="fa fa-times" aria-hidden="true"></i></a></article></li>'
                                                    $('#todo-list').append(article)
                                                    $('#todo-input').val("")
                                                });
                                            });
                                        });
                                }                                
                            })
                        }
                    })
                })
                
            }
        })







    })
    // listening for the keypress
    $('#todo-input').keypress(function(e){
        if(e.which == 13){
            $('#add-todo').click();
        }
    });

    // to delete a todo
    $(document).on('click','.identifier',(event)=>{
        
        let id =event.target.id.split('-')
        var data = null;

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", function () {
        if (this.readyState === this.DONE) {
            console.log(this.responseText);
        }
        });
        xhr.open("DELETE", `https://api.trello.com/1/cards/${id[0]}/checkItem/${id[1]}?key=708a24f98ac0f1529100dec29f4cbc99&token=6580601a5882c52eea871796c98bc49dbb5e8c98d3d99795bee5f3ba19aadced`);
        xhr.send(data);
        $(event.target).closest('.task').closest('li').remove()
    })












    // function addCheckItemsToList(checkItems){
    //     checkItems.map(item=>{
    //         if(item.state==="incomplete")
    //             appendTodoOnList(item)
    //     })
    // }
    // function appendTodoOnList(item){
    //     var id = item.id
    //     var textBoxContent = item.name;
    //     var article = '<li data-id="'+ id + '"' +'"><article class="task display-flex flex-direction-row flex-space-between align-items-center"><section class="display-flex"><input type="checkbox" id='+'"'+id+'"'+'><label for='
    //     +'"'+id+'"'+'>'+textBoxContent+'</label></section><a href="#" data-id="' + id + '"' +'class="delete-todo"><i class="fa fa-times" aria-hidden="true"></i></a></article></li>'
    //     $('#todo-list').append(article)
    // }

});

